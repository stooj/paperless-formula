=========
paperless
=========

Install django stack with paperless (https://github.com/the-paperless-project/paperless) configured.

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.


Available states
================

.. contents::
    :local:

``paperless``
-------------

TODO - add description of this state

``paperless.install``
------------------

Handles installation of paperless

``paperless.config``
------------------

Handles configuration of paperless


Template
========

This formula was created from a cookiecutter template.

See https://github.com/mitodl/saltstack-formula-cookiecutter.
