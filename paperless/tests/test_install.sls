{% from "paperless/map.jinja" import paperless with context %}

{% for pkg in paperless.pkgs %}
test_{{pkg}}_is_installed:
  testinfra.package:
    - name: {{ pkg }}
    - is_installed: True
{% endfor %}
