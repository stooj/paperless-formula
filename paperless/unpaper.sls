{% from "paperless/map.jinja" import paperless with context %}

paperless_unpaper_installed:
  pkg.installed:
    - pkgs: {{ paperless.pkgs.unpaper|yaml }}
