{% from "paperless/map.jinja" import paperless with context %}
{% set os = salt.grains.get('os', None) %}

{% set check_cert_cmd = '/usr/bin/certbot renew --dry-run --cert-name' %}
{% set create_cert_cmd = '/usr/bin/certbot' %}
{% set domain = paperless.app.domain %}
{% set letsencrypt_email = paperless.app.letsencrypt_email %}

{% if os == 'Ubuntu' %}
paperless_certbot_ppa_managed:
  pkgrepo.managed:
    - ppa: certbot/certbot
{% endif %}

paperless_certbot_installed:
  pkg.installed:
    - pkgs: {{ paperless.pkgs.certbot|yaml }}
    - require:
      - paperless_nginx_service_managed
    {% if os == 'Ubuntu' %}
      - paperless_certbot_ppa_managed
    {% endif %}

paperless_certificate_created:
  cmd.run:
    - unless: {{ check_cert_cmd }}
    - name: {{ create_cert_cmd }} certonly --quiet -d {{ domain }} --non-interactive --nginx --agree-tos --email {{ letsencrypt_email }}
    - require:
      - paperless_certbot_installed

paperless_certbot_timer_service_enabled:
  service.running:
    - name: {{ paperless.certbot.service }}
    - enable: true
