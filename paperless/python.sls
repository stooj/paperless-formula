{% from "paperless/map.jinja" import paperless with context %}

paperless_python_installed:
  pkg.installed:
    - pkgs: {{ paperless.pkgs.python|yaml }}
