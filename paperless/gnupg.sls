{% from "paperless/map.jinja" import paperless with context %}

paperless_gnupg_installed:
  pkg.installed:
    - pkgs: {{ paperless.pkgs.gnupg|yaml }}
