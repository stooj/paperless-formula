{% from "paperless/map.jinja" import paperless with context %}

paperless_libpoppler_dev_installed:
  pkg.installed:
    - pkgs: {{ paperless.pkgs.libpoppler_dev|yaml }}
