{% from "paperless/map.jinja" import paperless with context %}

paperless_gunicorn_systemd_service_managed:
  file.managed:
    - name: /etc/systemd/system/paperless-webserver.service
    - source: salt://paperless/templates/paperless-webserver.service.jinja
    - template: jinja
    - defaults:
        site: {{ paperless.app|yaml }}
    - context:
        venv_dir: {{ paperless.app.venv_directory }}
    - watch:
      - paperless_configuration_file_managed
      - paperless_source_cloned

paperless_gunicorn_access_log_permissions_managed:
  file.managed:
    - name: /var/log/access_paperless.log
    - user: {{ paperless.app.user_name }}
    - group: {{ paperless.app.user_name }}

paperless_gunicorn_error_log_permissions_managed:
  file.managed:
    - name: /var/log/error_paperless.log
    - user: {{ paperless.app.user_name }}
    - group: {{ paperless.app.user_name }}

paperless_gunicorn_service_managed:
  service.running:
    - name: paperless-webserver
    - enable: True

paperless_consumer_systemd_service_managed:
  file.managed:
    - name: /etc/systemd/system/paperless-consumer.service
    - source: salt://paperless/templates/paperless-consumer.service.jinja
    - template: jinja
    - defaults:
        site: {{ paperless.app|yaml }}
    - context:
        venv_dir: {{ paperless.app.venv_directory }}
    - watch:
      - paperless_configuration_file_managed
      - paperless_source_cloned

paperless_consumer_service_managed:
  service.running:
    - name: paperless-consumer
    - enable: True
