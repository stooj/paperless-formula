{% from "paperless/map.jinja" import paperless with context %}
{% set install_dir = paperless.app.install_dir %}
{% set venv_dir = paperless.app.venv_directory %}
{% set base_install_dir = install_dir.rsplit('/', 1)[0] %}
{% set base_venv_dir = venv_dir.rsplit('/', 1)[0] %}

paperless_group_present:
  group.present:
    - name: {{ paperless.app.user_name }}

paperless_user_present:
  user.present:
    - name: {{ paperless.app.user_name }}
    - home: {{ paperless.app.user_home }}
    - groups:
      - {{ paperless.app.user_name }}
    - require:
      - paperless_group_present

paperless_install_dir_created:
  file.directory:
    - name: {{ base_install_dir }}
    - user: {{ paperless.app.user_name }}
    - group: {{ paperless.app.user_name }}

paperless_venv_dir_created:
  file.directory:
    - name: {{ base_venv_dir }}
    - user: {{ paperless.app.user_name }}
    - group: {{ paperless.app.user_name }}

paperless_virtualenv_managed:
  virtualenv.managed:
    - name: {{ venv_dir }}
    - python: /usr/bin/python3
    - require:
      - paperless_python_installed
      - paperless_venv_dir_created

paperless_source_cloned:
  git.latest:
    - name: {{ paperless.app.git_source_url }}
    - rev: {{ paperless.app.git_source_release }}
    - target: {{ install_dir }}
    - user: {{ paperless.app.user_name }}
    - require: 
      - paperless_git_installed
      - paperless_virtualenv_managed
      - paperless_install_dir_created

paperless_requirements_installed:
  pip.installed:
    - bin_env: {{ venv_dir }}
    - requirements: {{ install_dir }}/requirements.txt
    - require:
      - paperless_virtualenv_managed
      - paperless_source_cloned
      - paperless_python_installed

paperless_configuration_file_managed:
  file.managed:
    - name: /etc/paperless.conf
    - source: salt://paperless/templates/paperless.conf.jinja
    - template: jinja
    - context:
        config: {{ paperless.app|yaml }}

paperless_consumption_directory_managed:
  file.directory:
    - name: /srv/consumption
    - user: {{ paperless.app.user_name }}
    - group: {{ paperless.app.user_name }}

paperless_database_migrations_applied:
  cmd.run:
    - name: {{ venv_dir }}/bin/python {{ install_dir }}/src/manage.py migrate
    - runas: {{ paperless.app.user_name }}
    - require: 
      - paperless_source_cloned
      - paperless_optipng_installed
    - onchanges:
      - paperless_source_cloned

{#
paperless_file_debug:
  file.serialize:
    - name: /tmp/stuff
    - dataset: {{ paperless|yaml }}
    - formatter: json
#}

paperless_superuser_created:
  file.managed:
    - name: {{ venv_dir }}/create_user.py
    - source: salt://paperless/templates/create_user.py.jinja
    - template: jinja
    - user: {{ paperless.app.user_name }}
    - context:
        python_bin: {{ venv_dir }}/bin/python
        username: {{ paperless.app.django_admin_username }}
        password: {{ paperless.app.django_admin_password }}
        email: {{ paperless.app.django_admin_email }}
  cmd.run:
    - name: {{ venv_dir }}/bin/python {{ install_dir }}/src/manage.py shell < {{ venv_dir }}/create_user.py
    - runas: {{ paperless.app.user_name }}
    - onchanges:
      - paperless_database_migrations_applied

paperless_static_collected:
  cmd.run:
    - name: {{ venv_dir }}/bin/python {{ install_dir }}/src/manage.py collectstatic --noinput
    - runas: {{ paperless.app.user_name }}
    - require: 
      - paperless_source_cloned
      - paperless_optipng_installed
      - paperless_database_migrations_applied
      - paperless_superuser_created
    - onchanges:
      - paperless_source_cloned
