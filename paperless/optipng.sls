{% from "paperless/map.jinja" import paperless with context %}

paperless_optipng_installed:
  pkg.installed:
    - pkgs: {{ paperless.pkgs.optipng|yaml }}
