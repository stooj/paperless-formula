{% from "paperless/map.jinja" import paperless with context %}

paperless_git_installed:
  pkg.installed:
    - pkgs: {{ paperless.pkgs.git|yaml }}
