{% from "paperless/map.jinja" import paperless with context %}

paperless_tesseract_installed:
  pkg.installed:
    - pkgs: {{ paperless.pkgs.tesseract|yaml }}
