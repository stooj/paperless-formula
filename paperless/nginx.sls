{% from "paperless/map.jinja" import paperless with context %}

paperless_nginx_installed:
  pkg.installed:
    - pkgs: {{ paperless.pkgs.nginx|yaml }}

paperless_nginx_default_site_disabled:
  file.absent:
    - name: /etc/nginx/sites-enabled/default

paperless_nginx_site_config_managed:
  file.managed:
    - name: /etc/nginx/sites-available/paperless
    - source: salt://paperless/templates/nginx.conf.jinja
    - template: jinja
    - context:
        site: {{ paperless.app|yaml }}
    - require:
      - paperless_nginx_installed

paperless_nginx_site_config_enabled:
  file.symlink:
    - name: /etc/nginx/sites-enabled/paperless
    - target: /etc/nginx/sites-available/paperless
    - require:
      - paperless_nginx_installed
      - paperless_nginx_site_config_managed

paperless_nginx_service_managed:
  service.running:
    - name: nginx
    - enable: True
    - watch:
      - paperless_nginx_site_config_managed
