{% from "paperless/map.jinja" import paperless with context %}

paperless_imagemagick_installed:
  pkg.installed:
    - pkgs: {{ paperless.pkgs.imagemagick|yaml }}

paperless_imagemagick_pdf_rights_adjusted:
  file.line:
    - name: /etc/ImageMagick-6/policy.xml
    - content: '<policy domain="coder" rights="read" pattern="PDF" />'
    - match: '<policy domain="coder" rights="none" pattern="PDF" />'
    - mode: replace
